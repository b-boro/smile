# smile

Super Metroid Integrated Level Editor

Archived from https://jathys.zophar.net/supermetroid/index.html. This repo is
for preservation and I have no intention of doing any development. The original
author described this as "open source" but didn't specify a license. They
describe the release like so:

>This was a transitional release. Incomplete, but as stable as SMILE ever gets.
>Had more things planned, but I get frustrated whenever I become motivated and a
>handful of people felt the need to beg, scold, and steal. SMILE is unfinished,
>but I will not be finishing it. SMILE is open sourced now. I would have
>released the source code over a year ago, but due to begging and stealing I've
>held onto it until now (as at least the begging has stopped). I might host
>copies of publicly released versions of SMILE, but will not guarantee that they
>are up to date. Despite the circumstances by which I am release the source, I
>do hope it will be worked on in earnest and progress swiftly.
